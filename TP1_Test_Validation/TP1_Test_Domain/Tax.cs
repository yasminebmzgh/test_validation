﻿using System;
namespace TP1_Test_Domain
{
	public class Tax
	{
        public double CalculateIncomeTax(double income)
        {
            double taxRate = 0.0;

            if (income >= 0 && income <= 17777)
            {
                taxRate = 0.0;
            }
            else if (income > 17777 && income <= 27478)
            {
                taxRate = 0.11;
            }
            else if (income > 27478 && income <= 78570)
            {
                taxRate = 0.30;
            }
            else if (income > 78570 && income <= 168994)
            {
                taxRate = 0.41;
            }
            else if (income > 168994)
            {
                taxRate = 0.45;
            }

            return taxRate * income;
        }
    }
}

