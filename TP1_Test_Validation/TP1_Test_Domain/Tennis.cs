﻿using System;
namespace TP1_Test_Domain
{
	public class Tennis
	{
        public string GetTennisScore(int player1, int player2)
        {
            if (player1 < 0 || player2 < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (player1 >= 4 && player2 >= 4)
            {
                if (player1 == player2)
                {
                    return "40-40";
                }
                else if (player1 == player2 + 1)
                {
                    return "Advantage player 1";
                }
                else if (player2 == player1 + 1)
                {
                    return "Advantage player 2";
                }
                else if (player1 >= player2 + 2)
                {
                    return "Winner: player 1";
                }
                else if (player2 >= player1 + 2)
                {
                    return "Winner: player 2";
                }
            }

            if (player1 == 3 && player2 < 3)
            {
                return "Winner: player 1";
            }
            else if (player2 == 3 && player1 < 3)
            {
                return "Winner: player 2";
            }

            var scorePlayer1 = GetScore(player1);
            var scorePlayer2 = GetScore(player2);

            return scorePlayer1 + "-" + scorePlayer2;
        }

        private string GetScore(int player)
        {
            switch (player)
            {
                case 0:
                    return "0";
                case 1:
                    return "15";
                case 2:
                    return "30";
                case 3:
                    return "40";
                default:
                    return "";
            }
        }
    }
}

