﻿using System;
using TP1_Test_Domain;
namespace TP1_Test_Unit_Test
{
	public class FizzBuzzTest
	{
        [Fact]
        public void DepassementLimiteInferieure()
        {

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => FizzBuzz.Generer(10));
        }
        [Fact]
        public void DepassementLimiteSuperieure()
        {

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => FizzBuzz.Generer(200));
        }


    }
}

