﻿using System;
using TP1_Test_Domain;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Xunit.Abstractions;

namespace TP1_Test_Unit_Test
{
	public class TaxTest
	{
        [Fact]
        public void TestCalculateIncomeTax_LowerBound()
        {
            // Arrange
            Tax tax = new Tax();
            double income = 0;
            // Act
            double taxtest = tax.CalculateIncomeTax(income);
            // Assert
            Assert.Equal(0, taxtest, 0.01);
        }


        [Theory]
        [InlineData(0, 0)]
        [InlineData(10777, 0)]
        [InlineData(27478, 0.11)]
        [InlineData(78570, 0.30)]
        [InlineData(168994, 0.41)]
        [InlineData(168995, 0.45)]
        public void TestCalculateTax(double income, double expectedRate)
        {
            Tax tax = new Tax();
            double expectedAmount = income * expectedRate;

            double result = tax.CalculateIncomeTax(income);
            Assert.Equal(expectedAmount, result, precision: 2);
        }
    }
}

