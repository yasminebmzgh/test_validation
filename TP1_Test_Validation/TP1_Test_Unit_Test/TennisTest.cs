﻿using System;
using TP1_Test_Domain;

namespace TP1_Test_Unit_Test
{
	public class TennisTest
	{
        [Theory]
        [InlineData(0, 0, "0-0")]
        [InlineData(1, 0, "15-0")]
        [InlineData(2, 0, "30-0")]
        [InlineData(5, 5, "40-40")]
        [InlineData(0, 1, "0-15")]
        [InlineData(0, 2, "0-30")]
        [InlineData(4, 5, "Advantage player 2")]
        [InlineData(3, 1, "Winner: player 1")]
        [InlineData(4, 6, "Winner: player 2")]
        public void TestScore(int player1, int player2, string expectedScore)
        {
            var tennis = new Tennis();
            var actualScore = tennis.GetTennisScore(player1, player2);
            Assert.Equal(expectedScore, actualScore);
        }

        [Fact]
        public void TestScore_ThrowsExceptionForNegativeScores()
        {
            var tennisGame = new Tennis();

            Assert.Throws<ArgumentOutOfRangeException>(() => tennisGame.GetTennisScore(-1, 0));
            Assert.Throws<ArgumentOutOfRangeException>(() => tennisGame.GetTennisScore(0, -1));
        }

    }
}

