﻿using System;
using TP2;
namespace TP2_Test
{
	public class Calculator_Test
	{
        [Theory]
        [InlineData(50000, 0.021, 108, 508.49)]
        [InlineData(75000, 0.035, 300, 375.46)]
        [InlineData(100000, 0.021, 108, 1016.98)]
        public void ShouldReturnMonthlyPayment(double amount, double interestRate, int durationInMonths, double expected)
        {
            Assert.Equal(expected, Calculator.CalculateMonthlyPayment(amount, interestRate, durationInMonths), 2);
        }
        [Theory]
        [InlineData(false, false, false, false, false, 0.003)]
        [InlineData(true, false, false, false, false, 0.0025)]
        [InlineData(false, true, false, false, false, 0.0045)]
        [InlineData(false, false, true, false, false, 0.006)]
        [InlineData(false, false, false, true, false, 0.0025)]
        [InlineData(false, false, false, false, true, 0.0045)]
        [InlineData(true, true, true, true, true, 0.008)]

        public void ShouldReturnInsuranceRate(bool isAthlete, bool isSmoker, bool hasHeartProblems, bool isComputerEngineer, bool isFighterPilot, double expectedRate)
        {
            var actualRate = Calculator.InsuranceRate(isAthlete, isSmoker, hasHeartProblems, isComputerEngineer, isFighterPilot);
            Assert.Equal(expectedRate, actualRate, 3);
        }
        [Fact]
        public void ShouldReturnInsuranceMonthlyPayment()
        {
            Assert.Equal(125, Calculator.CalculateInsuranceMonthlyPayment(50000, 0.03));
        }
        [Fact]
        public void ShouldCalculateTotalMonthlyPayment()
        {
            Assert.Equal(633.49, Calculator.CalculateTotalMonthlyPayment(508.49, 125));
        }
        [Fact]
        public void ShouldCalculateTotalInterest()
        {
            var res = Calculator.CalculateTotalInterest(500, 50000, 120);
            Assert.Equal(10000, res);
        }

    }
}

