﻿using TP2;

Console.WriteLine("Bienvenue dans le programme de calcul de prêt.");

// Prompt the user for the loan amount, interest rate, and duration in months
Console.Write("Entrez le montant du prêt : ");
double amount = double.Parse(Console.ReadLine());

Console.Write("Entrez le taux d'intérêt annuel en pourcentage : ");
double interestRate = double.Parse(Console.ReadLine()) / 100;

Console.Write("Entrez la durée en mois : ");
int durationInMonths = int.Parse(Console.ReadLine());

// Prompt the user for insurance options
Console.Write("Êtes-vous un athlète (O/N) ? ");
bool isAthlete = Console.ReadLine().ToUpper() == "O";

Console.Write("Êtes-vous fumeur (O/N) ? ");
bool isSmoker = Console.ReadLine().ToUpper() == "O";

Console.Write("Avez-vous des problèmes cardiaques (O/N) ? ");
bool hasHeartProblems = Console.ReadLine().ToUpper() == "O";

Console.Write("Êtes-vous ingénieur en informatique (O/N) ? ");
bool isComputerEngineer = Console.ReadLine().ToUpper() == "O";

Console.Write("Êtes-vous pilote de chasse (O/N) ? ");
bool isFighterPilot = Console.ReadLine().ToUpper() == "O";

// Calculate the monthly payment, insurance monthly payment, total monthly payment, total interest, and total insurance
double monthlyPayment = Calculator.CalculateMonthlyPayment(amount, interestRate, durationInMonths);
double insuranceRate = Calculator.InsuranceRate(isAthlete, isSmoker, hasHeartProblems, isComputerEngineer, isFighterPilot);
double insuranceMonthlyPayment = Calculator.CalculateInsuranceMonthlyPayment(amount, insuranceRate);
double totalMonthlyPayment = Calculator.CalculateTotalMonthlyPayment(monthlyPayment, insuranceMonthlyPayment);
double totalInterest = Calculator.CalculateTotalInterest(totalMonthlyPayment, amount, durationInMonths);
double totalInsurance = insuranceMonthlyPayment * durationInMonths;

// Calculate the amount of the loan that will be paid off after 10 years (120 months)
double amountPaidOff = monthlyPayment * 120;

// Display the results
Console.WriteLine($"Le montant mensuel à payer est de {monthlyPayment:C}.");
Console.WriteLine($"Le montant mensuel de l'assurance est de {insuranceMonthlyPayment:C}.");
Console.WriteLine($"Le montant total des intérêts remboursés est de {totalInterest:C}.");
Console.WriteLine($"Le montant total de l'assurance est de {totalInsurance:C}.");
Console.WriteLine($"Le montant du capital remboursé après 10 ans est de {amountPaidOff:C}.");

Console.WriteLine("Appuyez sur n'importe quelle touche pour quitter le programme.");
Console.ReadKey();