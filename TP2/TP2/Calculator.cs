﻿using System;
using System.Reflection.Metadata;

namespace TP2
{
	public class Calculator
	{
        public static double CalculateMonthlyPayment(double amount, double interestRate, int durationInMonths)
        {
            double monthlyInterestRate = interestRate / 12;
            double powerFactor = Math.Pow(1 + monthlyInterestRate, -durationInMonths);
            double monthlyPayment = (amount * monthlyInterestRate) / (1 - powerFactor);
            return Math.Round(monthlyPayment, 2, MidpointRounding.ToZero);
        }
        public static double CalculateInsuranceMonthlyPayment(double amount, double insuranceRate)
        {
            return amount * (insuranceRate / 12);
        }
        public static double CalculateTotalMonthlyPayment(double monthlyPayment, double monthlyInsurancePayment)
        {
            return monthlyPayment + monthlyInsurancePayment;
        }
        public static double CalculateTotalInterest(double totalMonthlyPayment, double amount, int duration)
        {
            return totalMonthlyPayment * duration - amount;
        }
        public static double InsuranceRate(bool isAthlete, bool isSmoker, bool hasHeartProblems, bool isComputerEngineer, bool isFighterPilot)
        {
            double rate = 0.003;
            if (isAthlete)
            {
                rate -= 0.0005;
            }
            if (isSmoker)
            {
                rate += 0.0015;
            }
            if (hasHeartProblems)
            {
                rate += 0.003;
            }
            if (isComputerEngineer)
            {
                rate -= 0.0005;
            }
            if (isFighterPilot)
            {
                rate += 0.0015;
            }

            return Math.Round(rate, 4);
        }

    }
}

